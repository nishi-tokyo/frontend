import React from 'react';
import { Navbar } from 'react-bootstrap';

function CustomNavbar({ loggedIn, username, onLogout }) {

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home">Welcome to library</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
    </Navbar>
  );
}

export default CustomNavbar;