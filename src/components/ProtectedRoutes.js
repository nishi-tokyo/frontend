import { Outlet, Navigate } from 'react-router-dom'

const ProtectedRoutes = ({authUser}) => {
    return authUser ? <Outlet/> : <Navigate to="/login"/>;
}

export default ProtectedRoutes