import { useRef, useState, useEffect } from "react";
import axios from '../api/axios';
import { useAuth } from "../context/AuthProvider";
import { Link } from "react-router-dom";
const LOGIN_URL = '/login';

const Login = () => {

    const userRef = useRef();
    const errRef = useRef();

    const {
        authUser,
        setAuthUser,
        isLoggedIn,
        setIsLoggedIn
    } = useAuth()
    
    const [user, setUser] = useState('')
    const [pwd, setPwd] = useState('')
    const [errMsg, setErrMsg] = useState('')

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post(LOGIN_URL,
                { 'username': user, 'password': pwd }
            )

            setAuthUser({
                name: user,
                roles: response?.data?.roles,
                token: response?.data?.token
            });
            setIsLoggedIn(true);
        } catch (err) {
            console.log(err)
            if (!err?.response) { 
                setErrMsg('No server response')
            } else if(err.response?.status === 401){
                setErrMsg('Unauthorized')
            } else if(err.response?.status === 404){
                setErrMsg('User not found')
            } else {
                setErrMsg('Login failed')
            }
        }
    }

    return (
        <> {isLoggedIn ? (
            <div>
                <h1>You are logged in</h1>
                <h1>User name: {authUser.name}</h1>
                <Link to="/home">Go to home</Link>
            </div>
        ) :(
            <section>
                <p ref={errRef} className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
                <h1>Login</h1>
                <form onSubmit={handleSubmit}>
                    <label htmlFor="username">
                        Username:
                        <input
                            type="text"
                            id="username"
                            value={user}
                            onChange={(e) => setUser(e.target.value)}
                            required
                        />
                    </label>
                    <label htmlFor="password">
                        Password
                        <input
                            type="password"
                            id="password"
                            value={pwd}
                            onChange={(e) => setPwd(e.target.value)}
                        />
                    </label>
                    <button type="submit">Log in</button>
                </form>
            </section>

        )}
        </>
    )
}

export default Login