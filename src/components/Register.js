import { useState } from "react";
import axios from '../api/axios';

const REGISTER_URL = '/accounts';

const Register = () => {
    const [name, setName] = useState('')
    const [pwd, setPwd] = useState('')
    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {
            'name': name,
            'password': pwd,
            'roles': 'ROLE_ADMIN'
        };
        console.log(pwd)
        const response = await axios.post(REGISTER_URL, data);
        console.log(response);
        
    }

   return (
    <div>
        Register
        <form onSubmit={handleSubmit}>
            <label>
                Name
                <input
                    type="text"
                    id="name"
                    value={name} 
                    onChange={(e) => setName(e.target.value)}   
                />
            </label>
            <label>
                Password
                <input
                    type="password"
                    id="password"
                    value={pwd}
                    onChange={(e) => setPwd(e.target.value)}     
                />
            </label>
            <button type="submit">Sign Up</button>
        </form>
    </div>
   )
}

export default Register