import { createContext, useState, useContext, useEffect } from "react";
const AuthContext = createContext({});

export const useAuth = () => {
    return useContext(AuthContext);
}

//Children which is wrapped with this AuthProvider have access to the value
export const AuthProvider = ({ children }) => {
    const [authUser, setAuthUser] = useState(null);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const value = {
        authUser,
        setAuthUser,
        isLoggedIn,
        setIsLoggedIn
    }

    return (
        <AuthContext.Provider value={value}>
            {children} 
        </AuthContext.Provider>
    )
}

export default AuthContext;
