import './App.css';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Register from './components/Register.js';
import Login from './components/Login.js';
import CustomNavbar from './components/CustomNavbar.js';
import ProtectedRoutes from './components/ProtectedRoutes.js';
import Home from './components/Home.js';
import { useAuth } from "./context/AuthProvider";

function App() {
  const { authUser } = useAuth()
  return (
      <div className="App">
        <CustomNavbar/>
        <Router>
          <Routes>
            <Route element={<ProtectedRoutes authUser={authUser}/>}>
              <Route element={<Home/>} path="/home"></Route>
            </Route>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Routes>
        </Router>
      </div>
  );
}

export default App;
